#pragma once

#include "message_handler.hpp"
#include "sync_event_demultiplexer.hpp"
#include "thread_pool.hpp"

#include <map>
#include <memory>

class Reactor {
public:
  explicit Reactor(uint16_t port_no) : port_no{port_no} {}

  void register_handler(std::unique_ptr<MessageHandler>&& handler) {
    handlers.insert({handler->get_header(), std::move(handler)});
  }

  void handle_events() {
    SyncEventDemultiplexer listener{port_no};
    listener.start();

    auto connection_handler = [this](Handle&& handle) {
      auto message = handle.read();
      if (!message.empty()) {
        const auto msg_id = message[0];
        auto handler = handlers.find(msg_id);
        if (handler != handlers.end()) {
          // hack to copy unique_ptr to the std::function
          auto pack = std::make_shared<Data>(handler->second->clone(),
                                             std::move(handle));
          auto worker = [message(std::move(message)),
                         pack(std::move(pack))]() mutable {
            pack->concrete_handler->handle(std::move(message),
                                           std::move(pack->handle));
          };
          pool.push(std::move(worker));
        }
      }
    };

    while (true) {
      listener.listen(connection_handler);
    }
  }

private:
  struct Data {
    Data(std::unique_ptr<MessageHandler>&& handler, Handle&& handle) :
        concrete_handler(std::move(handler)),
        handle(std::move(handle)){};

    std::unique_ptr<MessageHandler> concrete_handler;
    Handle handle;
  };

  ThreadPool pool;
  std::map<uint8_t, std::unique_ptr<MessageHandler>> handlers;
  uint16_t port_no{};
};
