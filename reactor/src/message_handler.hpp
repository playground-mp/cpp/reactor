#pragma once

#include <functional>
#include <iostream>
#include <memory>

#include "handle.hpp"
#include "tools.hpp"

class MessageHandler {
public:
  explicit MessageHandler() = default;
  MessageHandler(const MessageHandler&) = delete;
  MessageHandler& operator=(const MessageHandler&) = delete;
  MessageHandler(MessageHandler&&) = default;
  MessageHandler& operator=(MessageHandler&&) = delete;
  virtual ~MessageHandler() = default;

  [[nodiscard]] virtual uint8_t get_header() const = 0;
  virtual void handle(std::vector<uint8_t>&& message, Handle&& handle) = 0;

  [[nodiscard]] std::unique_ptr<MessageHandler> clone() const {
    return std::unique_ptr<MessageHandler>(clone_impl());
  }

private:
  [[nodiscard]] virtual MessageHandler* clone_impl() const = 0;
};

class EchoReq : public MessageHandler {
public:
  [[nodiscard]] uint8_t get_header() const override {
    const uint8_t echo_req_id{1};
    return echo_req_id;
  }

  void handle(std::vector<uint8_t>&& message, Handle&& handle) override {
    const uint8_t echo_resp_id{2};
    message[0] = echo_resp_id;
    delay();
    log_message("EchoReq", message[3], message[2]);
    handle.write(std::move(message));
    handle.close();
  }

  [[nodiscard]] EchoReq* clone_impl() const override {
    return std::make_unique<EchoReq>().release();
  }
};

class CreateSessionReq : public MessageHandler {
public:
  [[nodiscard]] uint8_t get_header() const override {
    const uint8_t create_session_req_id{32};
    return create_session_req_id;
  }

  void handle(std::vector<uint8_t>&& message, Handle&& handle) override {
    const uint8_t create_session_resp_id{33};
    message[0] = create_session_resp_id;
    delay();
    log_message("CreateSessionReq", message[3], message[2]);
    handle.write(std::move(message));
    handle.close();
  }

  [[nodiscard]] CreateSessionReq* clone_impl() const override {
    return std::make_unique<CreateSessionReq>().release();
  }
};

class CreateBearerReq : public MessageHandler {
public:
  [[nodiscard]] uint8_t get_header() const override {
    const uint8_t create_bearer_resp_id{95};
    return create_bearer_resp_id;
  }

  void handle(std::vector<uint8_t>&& message, Handle&& handle) override {
    const uint8_t create_bearer_resp_id{96};
    message[0] = create_bearer_resp_id;
    log_message("CreateBearerReq", message[3], message[2]);
    handle.write(std::move(message));
    handle.close();
  }

  [[nodiscard]] CreateBearerReq* clone_impl() const override {
    return std::make_unique<CreateBearerReq>().release();
  }
};
