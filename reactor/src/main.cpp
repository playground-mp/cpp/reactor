
#include "message_handler.hpp"
#include "reactor.hpp"

#include <memory>

int main() {
  const uint16_t port_no{8888};
  try {
    Reactor reactor{port_no};
    reactor.register_handler(std::make_unique<EchoReq>());
    reactor.register_handler(std::make_unique<CreateSessionReq>());
    reactor.register_handler(std::make_unique<CreateBearerReq>());
    reactor.handle_events();
  } catch (...) {
  }
  return 0;
}
