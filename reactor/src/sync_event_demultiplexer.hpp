#pragma once

#include "handle.hpp"

#include <functional>
#include <memory>

#include <boost/asio.hpp>

class SyncEventDemultiplexer {
public:
  explicit SyncEventDemultiplexer(uint16_t port_no) : port_no{port_no} {}

  void start() {
    acceptor = std::make_unique<boost::asio::ip::tcp::acceptor>(
        io_service,
        boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), port_no));
  }

  void listen(std::function<void(Handle)>&& notification_handler) {
    auto socket = boost::asio::ip::tcp::socket(io_service);
    acceptor->accept(socket);
    notification_handler(Handle(std::move(socket)));
  }

private:
  boost::asio::io_service io_service;
  std::unique_ptr<boost::asio::ip::tcp::acceptor> acceptor{};

  uint16_t port_no{};
};
