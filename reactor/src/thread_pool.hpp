#pragma once

#include "function_pool.hpp"

#include <functional>
#include <thread>
#include <vector>

class ThreadPool {
public:
  ThreadPool() { init(); }

  void init() {
    for (size_t i = 0; i < std::thread::hardware_concurrency(); ++i) {
      threads.emplace_back(&FunctionPool::loop, &function_pool);
    }
  }

  void push(std::function<void()>&& func) {
    function_pool.push(std::move(func));
  }

private:
  FunctionPool function_pool;
  std::vector<std::thread> threads;
};
