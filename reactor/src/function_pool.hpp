#pragma once

#include <atomic>
#include <condition_variable>
#include <functional>
#include <mutex>
#include <queue>

#include "handle.hpp"

class FunctionPool {
public:
  void push(std::function<void()>&& func) {
    std::lock_guard<std::mutex> lock(mutex);
    workers.push(std::move(func));
    condition_variable.notify_one();
  }

  void done() {
    std::lock_guard<std::mutex> lock(mutex);
    is_working = false;
    condition_variable.notify_all();
  }

  void loop() {
    std::function<void()> func;
    while (true) {
      {
        std::unique_lock<std::mutex> lock(mutex);
        condition_variable.wait(
            lock, [this]() { return !workers.empty() || !is_working; });

        if (!is_working && workers.empty()) {
          return;
        }

        func = workers.front();
        workers.pop();
      }
      func();
    }
  }

private:
  std::queue<std::function<void()>> workers;
  std::mutex mutex;
  std::condition_variable condition_variable;
  std::atomic<bool> is_working{true};
};
