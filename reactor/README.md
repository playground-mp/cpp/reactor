
### Reactor

The Modern C++ variation of Reactor design pattern with thread pool for execution of concrete handler tasks.

```mermaid
sequenceDiagram
  participant MainProgram
  participant Reactor
  participant SyncEventDemultiplexer
  participant Handle
  participant ThreadPool
  participant FunctionPool

  MainProgram->>Reactor: register_handler()
  Reactor-->>MainProgram: 
    
  MainProgram->>Reactor: handle_events()

  Reactor->>SyncEventDemultiplexer: start()
  SyncEventDemultiplexer-->>Reactor: 

  loop    
    Reactor->>SyncEventDemultiplexer: listen(notification_handler)
    SyncEventDemultiplexer->>SyncEventDemultiplexer: accept()
    SyncEventDemultiplexer->>Handle: ctor(socket)
    Handle-->>SyncEventDemultiplexer: 
    SyncEventDemultiplexer-->>Reactor: notification_handler(handle)
    Reactor->>ThreadPool: push(std::function) 
    Note over Reactor, ThreadPool: std::function wrap the concrete handler
    ThreadPool->>FunctionPool: push(std::function)
    Note over ThreadPool, FunctionPool: std::function will be executed by free thread from pool
  end
```

### Targets

* `make`
* `make clang-tidy`
* `make clang-format`
* `{ ./reactor > /dev/null & }; make test; pgrep reactor | xargs kill -9`


### Tools

`c++17`, `boost`, `python3.7`, `pytest`, `clang-tidy-10`, `clang-format-10`

