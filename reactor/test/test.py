import pytest
import socket

HOST = 'localhost'
PORT = 8888

def test_echo_request():
  
  s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
  s.connect((HOST, PORT))
  
  msg = b'\x01\x01\x01\x01'
  s.sendall(msg)
  data = s.recv(4)
  s.close()
  
  assert data == b'\x02\x01\x01\x01'

def test_create_session_request():
  
  s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
  s.connect((HOST, PORT))
  
  msg = b'\x20\x01\x01\x01'
  s.sendall(msg)
  data = s.recv(4)
  s.close()
  
  assert data == b'\x21\x01\x01\x01'

def test_create_bearer_request():
  
  s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
  s.connect((HOST, PORT))
  
  msg = b'\x5F\x01\x01\x01'
  s.sendall(msg)
  data = s.recv(4)
  s.close()
  
  assert data == b'\x60\x01\x01\x01'


import asyncio
import struct

async def async_echo_request(id):
  reader, writer = await asyncio.open_connection(
      HOST, PORT)

  msg = b'\x01\x01' + struct.pack('<H', id)
  writer.write(msg)
  print ("Sent EchoRequest")
  await writer.drain()

  resp = await reader.read(4)

  writer.close()
  assert resp == b'\x02\x01' + struct.pack('<H', id)

async def async_create_session_request(id):
  reader, writer = await asyncio.open_connection(
      HOST, PORT)

  msg = b'\x20\x01' + struct.pack('<H', id)
  writer.write(msg)
  print ("Sent CreateSessionRequest")
  await writer.drain()

  resp = await reader.read(4)

  writer.close()
  assert resp == b'\x21\x01' + struct.pack('<H', id)

async def async_create_bearer_request(id):
  reader, writer = await asyncio.open_connection(
      HOST, PORT)

  msg = b'\x5F\x01' + struct.pack('<H', id)
  writer.write(msg)
  print ("Sent CreateBearerRequest")
  await writer.drain()

  resp = await reader.read(4)

  writer.close()
  assert resp == b'\x60\x01' + struct.pack('<H', id)


def test_async_messages():
  jobs = list()
  loop = asyncio.get_event_loop()
  
  for id in range(1, 1024, 8):
    jobs.append(loop.create_task(async_echo_request(id)))
    jobs.append(loop.create_task(async_create_session_request(id)))
    jobs.append(loop.create_task(async_create_bearer_request(id)))

  for job in jobs:
    loop.run_until_complete(job)


